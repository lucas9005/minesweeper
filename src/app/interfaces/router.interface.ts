// Route
export interface Route {
    routerLink: string;
    icon: string;
    label: string;
}
